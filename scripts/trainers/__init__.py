# -*- coding:utf-8 -*-

import time, math
from tqdm import tqdm, trange


class Trainer:
    
    def __init__(self, train_dataloader, valid_dataloader, model, config, export = True):
        self.config = config
        self.export =export
        self.train_dataloader = train_dataloader
        self.valid_dataloader = valid_dataloader
        self.model = model
        self.config = config
        
        
        
        self.num_epochs = self.config['model_params']['max_epochs']
        
        if self.config['model_params']['checkpoint_model_file']:
            chk_pt_name = self.config['model_params']['checkpoint_model_file']
            chk_pt_list = chk_pt_name.split('_')
            ep_idx = int([i for i, x in enumerate(chk_pt_list) if x == 'EP'][0]) + 1
            gs_idx = int([i for i, x in enumerate(chk_pt_list) if x == 'GS'][0]) + 1
            self.starting_epoch = int(chk_pt_list[ep_idx])
            self.global_step = int(chk_pt_list[gs_idx])
            self.model.setup()
            self.net = getattr(self.model, 'net' + self.config['model_params']['model_name'])
        else:
            self.net = getattr(self.model, 'net' + self.config['model_params']['model_name'])
            self.global_step = 0
            self.starting_epoch = 0
        
        
    def evaluate_val(self):
        self.model.num_valid_examplaes = 0
        self.model.val_inputs = [[] for _ in range(len(self.model.input))]
        self.model.val_predictions = []
        self.model.val_labels = []
        self.model.val_loss = 0
        self.model.val_step = 0
        
        for data in tqdm(self.valid_dataloader, desc="Evaluating for {}".format('Evaluation')):
            self.model.set_input(data)
            self.model.validate()
    
    
    def clear_train_info(self):
        self.model.num_train_examples = 0
        self.model.train_inputs = []
        self.model.train_predictions = []
        self.model.train_labels = []
        
                    
    def __call__(self):
        global_step = self.global_step
        print('start training on device {}'.format(next(self.net.parameters()).device))
        print('starting epoch = {}, global_step = {}'.format(self.starting_epoch, global_step))
        train_iterator = trange(int(self.starting_epoch), int(self.num_epochs), desc="Epoch")
        for epoch, _ in enumerate(train_iterator):
            epoch_start_time = time.time()  # timer for entire epoch
            epoch_iterator = tqdm(self.train_dataloader, desc="Iteration")
            # Callback
            self.train_dataloader.dataset.pre_epoch_callback(epoch)
            self.model.pre_epoch_callback(epoch)
            self.clear_train_info()
            for i, data in enumerate(epoch_iterator):  # inner loop within one epoch   
                self.model.train()
                self.model.set_input(data)         # unpack data from dataset and apply preprocessing
                if i == 0:
                    self.model.train_inputs = [[] for _ in range(len(self.model.input))]
                self.model.forward()
                self.model.backward()
                self.model.update_train_example()
                
                # Update parameters
                if i % self.config['model_params']['gradient_accumulation_steps'] == 0:
                    self.model.optimize_parameters()   # calculate loss functions, get gradients, update network weights
                
                # Print logs
                if global_step % self.config['train_params']['log_interval'] == 0:
                    # evaluate validation
                    self.evaluate_val()
                    self.model.global_step_callback(epoch, global_step)
                    
                if global_step % self.config['train_params']['checkpoint_interval'] == 0:
                    self.model.save_networks(epoch, global_step)
                    self.model.save_optimizers(epoch, global_step)
                
                global_step += 1
            
            # evaluate validation
            self.evaluate_val()
            self.model.post_epoch_callback(epoch, global_step)
            self.train_dataloader.dataset.post_epoch_callback(epoch)
            self.model.update_learning_rate() # update learning rates every epoch

        self.model.tb_writer.close()