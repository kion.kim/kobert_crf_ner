# -*- coding:utf-8 -*-

import argparse
import copy
import numpy as np
import torch.nn.functional as F
import sentencepiece as spm
import json
from pathlib import Path
import random
import torch
import os
from sklearn.metrics import classification_report
from torch.utils.tensorboard import SummaryWriter

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', type=str, default='../config.json')
    return parser


def load_config():
    parser = get_args()
    args = parser.parse_args()
    with open(os.path.join(os.getcwd(), args.config), 'r') as f:
        _c = json.load(f)
    return _c


def f1_score(tar_path, pre_path, length, tag, tag_map):
    """
    params:
        tag_id_map: {v:k for k, v in tag_map.items()}
        tar_path: tensor Batch size * Number of Batch) * Max Length
        pre_path: tensor Batch size * Number of Batch) * Max Length
        tag: string
        tag_map: dictionary
    """
    origin = 0.
    found = 0.
    right = 0.
    for idx, fetch in enumerate(zip(tar_path, pre_path, length)):
        tar, pre, _length = fetch
        tar_tags = get_tags_BIE(tar[:_length], tag, tag_map)
        pre_tags = get_tags_BIE(pre[:_length], tag, tag_map)

        origin += len(tar_tags)
        found += len(pre_tags)

        for p_tag in pre_tags:
            if p_tag in tar_tags:
                right += 1

    recall = 0. if origin == 0 else (right / origin)
    precision = 0. if found == 0 else (right / found)
    f1 = 0. if recall+precision == 0 else (2*precision*recall)/(precision + recall)
    print("\t{}\trecall {:.2f}\tprecision {:.2f}\tf1 {:.2f}\tsupport {}".format(tag, recall, precision, f1, origin))
    return recall, precision, f1, origin


class CheckpointManager:
    def __init__(self, checkpoint_dir):
        print("checkpoint dir = {}".format(checkpoint_dir))
        if not isinstance(checkpoint_dir, Path):
            checkpoint_dir = Path(checkpoint_dir)
        checkpoint_dir.mkdir(parents=True, exist_ok=True)
        self._checkpoint_dir = checkpoint_dir
        print('model dir in checkpoint manager =', self._checkpoint_dir)

    def save_checkpoint(self, state, filename):
        print('actually save model at =', self._checkpoint_dir / filename)
        torch.save(state, self._checkpoint_dir / filename)

    def load_checkpoint(self, filename):
        state = torch.load(self._checkpoint_dir / filename, map_location=torch.device('cpu'))
        return state


class SummaryManager:
    def __init__(self, model_summary_dir):
        if not isinstance(model_summary_dir, Path):
            model_summary_dir = Path(model_summary_dir)
        model_summary_dir.mkdir(parents=True, exist_ok=True)
        self._model_summary_dir = model_summary_dir
        self._summary = {}

    def save(self, filename):
        with open(self._model_summary_dir / filename, mode='w') as io:
            json.dump(self._summary, io, indent=4)

    def load(self, filename):
        with open(self._model_summary_dir / filename, mode='r') as io:
            metric = json.loads(io.read())
        self.update(metric)

    def update(self, summary):
        self._summary.update(summary)

    def reset(self):
        self._summary = {}

    @property
    def summary(self):
        return self._summary

    
def save_mechanism(log_dir, checkpoint_dir, model_summary_dir):
    log_dir = Path(log_dir)
    checkpoint_dir = Path(checkpoint_dir)
    _tb_writer = SummaryWriter(log_dir/ 'runs')
    _checkpoint_manager = CheckpointManager(checkpoint_dir)
    _summary_manager = SummaryManager(model_summary_dir)
    return _tb_writer, _checkpoint_manager, _summary_manager

    
def set_seed(n_gpu, seed = 100):
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    n_gpu = torch.cuda.device_count()
    if n_gpu > 0:
        torch.cuda.manual_seed_all(seed)
        
        
import operator
import pandas as pd
def save_cr_and_cm(va_dm, list_of_y_real, list_of_pred_tags, y_real_list, y_pred_list, length_list, cr_save_path="classification_report.csv", cm_save_path="confusion_matrix.png"):
    """ print classification report and confusion matrix """

    sorted_ner_to_index = sorted(va_dm.tag_map.items(), key=operator.itemgetter(1))
    tag_to_remove = ['[CLS]', '[SEP]', '[PAD]', '[MASK]', 'B-O', 'I-O', 'E-O']
    target_names = []
    
    for ner_tag, index in sorted_ner_to_index:
        if ner_tag in tag_to_remove:
            continue
        else:
            target_names.append(ner_tag)

    label_index_to_print = list(range(len(tag_to_remove), len(va_dm.tag_map)))  # ner label indice except '[CLS]', '[SEP]', '[PAD]', '[MASK]' and 'O' tag
    print('=' * 100)
    print('Metrics in sequence')
    print('-' * 100)
    print(classification_report(y_true=list_of_y_real, y_pred=list_of_pred_tags, target_names=target_names, labels=label_index_to_print, digits=4))
    cr_dict = classification_report(y_true=list_of_y_real, y_pred=list_of_pred_tags, target_names=target_names, labels=label_index_to_print, digits=4, output_dict=True)
    
    print('=' * 100)
    print('Metrics in group')
    print('-' * 100)
    
    for tag in va_dm.tags:
        _rec, _prec, _f1, _supp = f1_score(y_real_list, y_pred_list, length_list, tag, va_dm.tag_map)
        cr_dict[tag + '_in_grp'] = {'precision': _prec, 'recall': _rec, 'f1-score': _f1, 'support': _supp}
            
    df = pd.DataFrame(cr_dict).transpose()
    df.to_csv(cr_save_path)
    np.set_printoptions(precision=2)
    plot_confusion_matrix(y_true=list_of_y_real, y_pred=list_of_pred_tags, classes=target_names, labels=label_index_to_print, normalize=False, title='Confusion matrix, without normalization')
    plt.savefig(cm_save_path)

    
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.utils.multiclass import unique_labels
def plot_confusion_matrix(y_true, y_pred, classes, labels,
                          normalize=False,
                          title=None,
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if not title:
        if normalize:
            title = 'Normalized confusion matrix'
        else:
            title = 'Confusion matrix, without normalization'

    # Compute confusion matrix
    cm = confusion_matrix(y_true=y_true, y_pred=y_pred, labels=labels)
    # Only use the labels that appear in the data

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    # --- plot 크기 조절 --- #
    plt.rcParams['savefig.dpi'] = 200
    plt.rcParams['figure.dpi'] = 200
    plt.rcParams['figure.figsize'] = [20, 20]  # plot 크기
    plt.rcParams.update({'font.size': 10})
    # --- plot 크기 조절 --- #

    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)

    # --- bar 크기 조절 --- #
    from mpl_toolkits.axes_grid1 import make_axes_locatable
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    plt.colorbar(im, cax=cax)
    # --- bar 크기 조절 --- #
    # ax.figure.colorbar(im, ax=ax)

    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()
    return ax


def get_tags_BIE(path, tag, tag_map):
    begin_tag = tag_map.get("B-" + tag)
    mid_tag = tag_map.get("I-" + tag)
    end_tag = tag_map.get("E-" + tag)
    o_list = [tag_map.get("B-O"), tag_map.get("I-O"), tag_map.get("E-O")]
    begin = -1
    end = 0
    tags = []
    last_tag = 0

    for index, tag in enumerate(path):
        if tag == begin_tag and index == 0:
            begin = 0
        elif tag == begin_tag:
            begin = index
        elif tag == end_tag and last_tag in [mid_tag, begin_tag] and begin > -1:
            end = index
            tags.append([begin, end])
        elif tag in o_list:
            if last_tag == begin_tag:
                tags.append([begin, begin])
            else:
                begin = -1
        last_tag = tag
    return tags


def transfer_to_device(x, device):
    """Transfers pytorch tensors or lists of tensors to GPU. This
        function is recursive to be able to deal with lists of lists.
    """
    if isinstance(x, list):
        for i in range(len(x)):
            x[i] = transfer_to_device(x[i], device)
    else:
        x = x.to(device)
    return x