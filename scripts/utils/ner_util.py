# -*- coding:utf-8 -*-

import numpy as np
from utils import get_tags_BIE
import re
import sentencepiece as spm
from collections import OrderedDict

def NER_decode(path, text, text_tok, id2tag, tag_map, tokenizer):

    # Get rid of unnecessary tokens
    detected_tag = [id2tag[tag] for tag in path[:len(text_tok) + 1] if tag not in [tag_map['B-O'], tag_map['I-O'], tag_map['E-O'], 
                                                                               tag_map['[CLS]'], tag_map['[SEP]'], tag_map['[PAD]'], 
                                                                               tag_map['[MASK]']]]
    #detected_tag = [id2tag[tag] for tag in path[:len(text_tok)]]
    print('text = {}'.format(text))
    print('tokens = {}'.format(text_tok))
    print('detected_tag_original = {}'.format([id2tag[tag] for tag in path[:len(text_tok)]]))
    print('detected_tag = {}'.format(detected_tag))
    detected_tag = np.unique([s.split('-')[1] for s in detected_tag]).tolist()
    
    print('unique detected_tag = {}'.format(detected_tag))
    
    result = OrderedDict()
    result['original_sentence'] = text
    _res = []
    for i, tag in enumerate(detected_tag):
        positions = get_tags_BIE(path[1: len(text_tok) + 1], tag, tag_map)
        for idx, (s,e) in enumerate(positions):
            entity = tokenizer.DecodePieces(text_tok[s:e+1])
            if len(entity) == 0:
                continue
            else:
                try:
                    entity_pos = re.search(pattern=entity, string=text).span()
                    
                    tmp_result = OrderedDict()
                    tmp_result['entity_type'] = re.sub(pattern = '[<>]+', repl = '', string=tag)    
                    tmp_result['entity_value'] = entity
                    tmp_result['start_pos'] = entity_pos[0]
                    tmp_result['end_pos'] = entity_pos[1]
                    #tmp_result['score'] = round(prob, 2)
                    tmp_result['isPredict'] = True
                    _res.append(tmp_result)
                except Exception as e:
                    print('error = {}'.format(e))
                    continue
    result['entities'] = _res
    return result