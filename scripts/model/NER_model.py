# -*- coding:utf-8 -*-


# tensorboard --logdir=. --bind_all


from __future__ import absolute_import, division, print_function, unicode_literals

from model.base_model import BaseModel
from model.kobert_crf import Kobert_CRF

import torch
from pytorch_transformers import AdamW, WarmupLinearSchedule
from utils import transfer_to_device, save_mechanism, f1_score
import time
import sentencepiece as spm

     
class NERModel(BaseModel):
    def __init__(self, model_config, train_config, **kwargs):
        """
        Initialize the model.
        """
        super().__init__(model_config, train_config, **kwargs)
        ## Parameters
        self.network_names = [model_config['model_name']] # Single network. Some networks have multiple networks, losses, and optimizers
        self.num_classes = model_config['num_classes']
        self.tags = kwargs['tag_info']['tags']
        self.tag_map = kwargs['tag_info']['tag_map']
        self.tag_id_map = {v:k for k, v in self.tag_map.items()}
        self.bert_pretrained_model_file = model_config['bert_pretrained_model_file']
        self.vocab_file = model_config['vocab_file']
        self.bert_config = kwargs['bert_config']
        self.gradient_accumulation_steps = model_config['gradient_accumulation_steps']
        self.learning_rate = model_config['learning_rate']
        self.adam_eps = model_config['adam_epsilon']
        self.warmup_steps = model_config['warmup_steps']
        self.tokenizer_model = model_config['tokenizer_model']
        self.loss_names = self.network_names   # Single loss

        ## Define network
        self.netNER = Kobert_CRF(self.num_classes, self.tag_map, self.bert_pretrained_model_file, self.vocab_file, self.bert_config)
        self.netNER = self.netNER.to(self.device)

        # Define optimizer & scheduler
        if self.is_train:  # only defined during training time
            self.t_total = kwargs['t_total']
            print('t total = ', self.t_total)
            self.optimizer = self.define_optimizer()
            self.optimizers = [self.optimizer]
            self.scheduler = self.define_scheduler()
            self.schedulers = [self.scheduler]
            


    def forward(self):
        """Run forward pass.
        """
        #self.output = self.netNER.neg_log_likelihood(*self.input, self.label)
        self.output = self.netNER(*self.input)

    def backward(self):
        """Calculate losses; called in every training iteration.
        """
        tmp_loss, _ = self.netNER(*self.input, self.label)
        setattr(self, 'loss_' + self.loss_names[0], -tmp_loss)
        

    def set_input(self, input):
        """
        Unpack input data from the dataloader and perform necessary pre-processing steps.
        The implementation here is just a basic setting of input and label. You may implement
        other functionality in your own model.
        x_input, token_type_ids, y_real: torch.Size([batch_size * max_len])
        """
        x_input, token_type_ids, y_real, length = input[:4] 
        self.input = transfer_to_device([x_input, token_type_ids, length], self.device)
        self.label = transfer_to_device(y_real, self.device)
        
        
    def validate(self):
        super().validate() # run the forward pass
        

        
    def global_step_callback(self, epoch, global_step):
        self.evaluate(global_step)
        print('[Epoch: {0} Global Step: {1}]: val_Loss = {2:.3f}\tval_accuracy = {3:.3f}'.format(
            epoch, 
            global_step, 
            float(self.val_loss / self.num_valid_examples),
            float(self.val_acc)
        ))
        
        
    def pre_epoch_callback(self, epoch):
        self.epoch_start_time = time.time()
        print('epoch {} start'.format(epoch))
        
        
    def post_epoch_callback(self, epoch, global_step):
        print('End of epoch {} / {}\tglobal step: {}\ttrain loss = {:.3f}\tvalid loss = {:.3f}\ttrain accuracy = {:.3f}\tvalidate accuracy = {:.3f}\tTime Taken: {:.3f} min'.format(
                                                                                                            epoch, 
                                                                                                            self.model_config['epochs'],
                                                                                                            global_step,
                                                                                                            float(self.train_loss / self.num_train_examples),
                                                                                                            float(self.val_loss / self.num_valid_examples),
                                                                                                            float(self.train_acc),
                                                                                                            float(self.val_acc),
                                                                                                            (time.time() - self.epoch_start_time)/60)
        )
        #cr_save_path = model_summary_dir / 'best-epoch-{}-step-{}-acc-{:.3f}-cr.csv'.format(epoch + 1, global_step, best_dev_acc)
        #cm_save_path = model_summary_dir / 'best-epoch-{}-step-{}-acc-{:.3f}-cm.png'.format(epoch + 1, global_step, best_dev_acc)
        #save_cr_and_cm(va_dm, list_of_y_real, list_of_pred_tags, y_real_list, y_pred_list, list_of_length, cr_save_path=cr_save_path, cm_save_path=cm_save_path)


    
    def define_optimizer(self):
        """
        decide parameters to learn
        define scheduler if want
        Use 1 gpu for default
        """
        param_optimizer = list(self.netNER.named_parameters())
        print("Number of parameter matrix = ", len(param_optimizer))

        no_decay = ['bias', 'LayerNorm.bias', 'LayerNorm.weight']
        optimizer_grouped_parameters = [
            {'params': [p for n, p in param_optimizer if not any(nd in n for nd in no_decay)], 'weight_decay': .01},
            {'params': [p for n, p in param_optimizer if any(nd in n for nd in no_decay)], 'weight_decay': .00},
        ]
        _opt = AdamW(optimizer_grouped_parameters, lr = self.learning_rate, eps = self.adam_eps)
        return _opt
    
    
    def define_scheduler(self):
        # WarmupLinearSchedule: Linearly increases learning rate from 0 to 1 over warmup fraction of training steps. Linearly decreases learning rate from 1. to 0. over remaining 1
        return WarmupLinearSchedule(self.optimizer, warmup_steps = self.warmup_steps, t_total = self.t_total)
    
    

        
    def evaluate(self, step, n_sample = 2000):
        super().evaluate(step, n_sample)
        
        ## Write text info
        self.tokenizer = spm.SentencePieceProcessor()
        self.tokenizer.Load(self.tokenizer_model)
        print('val_inputs in evaluate = '.format(len(self.val_inputs[0])))
        for i in range(100):
            
            id = self.tokenizer.DecodePieces(
                [self.tokenizer.IdToPiece(x) for x in self.val_inputs[0][i]]
            ) + '>>>' + 'token: ' + ' '.join([self.tokenizer.IdToPiece(x) for x in self.val_inputs[0][i]])
            
            pred = '  '.join([self.tag_id_map[x]for x in self.val_predictions[i]])
            label = '  '.join([self.tag_id_map[x] for x in self.val_labels[i]])
            print('pred = ', pred)
            self.tb_writer.add_text(id, 'pred: ' + pred, step)
            self.tb_writer.add_text(id, 'label: ' + label, step)        
        
        for tag in self.tags:
            rec, prec, f1, support = f1_score(self.val_labels[:n_sample], self.val_predictions[:n_sample], self.val_inputs[2][:n_sample], tag, self.tag_map)
            self.tb_writer.add_scalars(tag, {'recall': rec, 'precision': prec, 'f1 score': f1}, step)
