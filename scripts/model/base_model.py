import os
import torch
#from utils import get_scheduler
from utils import transfer_to_device, save_mechanism
from collections import OrderedDict
from abc import ABC, abstractmethod
from pathlib import Path


class BaseModel(ABC):
    """This class is an abstract base class (ABC) for models.
    """

    def __init__(self, model_config, train_config, **kwargs):
        """Initialize the BaseModel class.
        Parameters:
            config: config dictionary.
        When creating your custom class, you need to implement your own initialization.
        In this fucntion, you should first call <BaseModel.__init__(self, opt)>
        Then, you need to define these lists:
            -- self.network_names (str list):       define networks used in our training.
            -- self.optimizers (optimizer list):    define and initialize optimizers. You can define one optimizer for each network. If two networks are updated at the same time, you can use itertools.chain to group them. See cycle_gan_model.py for an example.
        """
        self.model_config = model_config
        self.train_config  = train_config
        self.is_train = train_config['is_train']
        self.use_cuda = train_config['cuda']
        self.device = torch.device('cpu') # Just set default
        self.epochs = model_config['epochs']
        self.is_train = train_config['is_train']
        if len(train_config['gpu_id']) == 1: 
            self.device = torch.device('cuda:{}'.format(train_config['gpu_id'][0]))
        self.tb_writer, self.checkpoint_manager, self.summary_manager = save_mechanism(train_config['log_dir'], train_config['checkpoint_dir'], train_config['model_summary_dir'])
        
        torch.backends.cudnn.benchmark = True
        self.save_dir = Path(train_config['checkpoint_dir'])
        self.network_names = []
        self.loss_names = []
        self.optimizers = []
        self.visual_names = []
        
        # storing predictions and labels for validation
        self.val_step = 0
        self.val_loss = 0
        self.num_valid_examples = 0
        self.val_acc = 0
        self.val_inputs = []
        self.val_predictions = []
        self.val_labels = []
        
        self.train_step = 0
        self.train_loss = 0
        self.train_acc = 0
        self.num_train_examples = 0
        self.train_inputs = []
        self.train_predictions = []
        self.train_labels = []
        

    @abstractmethod  
    def set_input(self, input):
        """
        Unpack input data from the dataloader and perform necessary pre-processing steps.
        The implementation here is just a basic setting of input and label. You may implement
        other functionality in your own model.
        """
        pass


    @abstractmethod
    def forward(self):
        """Run forward pass; called by both functions <optimize_parameters> and <test>."""
        pass

    
    @abstractmethod
    def optimize_parameters(self):
        """Calculate losses, gradients, and update network weights; called in every training iteration"""
        pass

    
    def setup(self):
        """Load and print networks; create schedulers.
        """
        # enable restarting training
        self.load_networks(self.model_config['checkpoint_model_file'])
        if self.is_train:
            if self.model_config['optimizer_file']:
                self.load_optimizers(self.model_config['optimizer_file'])
                for o in self.optimizers:
                    o.param_groups[0]['lr'] = o.param_groups[0]['initial_lr'] # reset learning rate
            print('optimizer is not dprovided!!!!*****')
        #self.schedulers = [get_scheduler(optimizer, self.config) for optimizer in self.optimizers]

        chk_pt_name = self.model_config['checkpoint_model_file']
        chk_pt_list = chk_pt_name.split('_')
        ep_idx = int([i for i, x in enumerate(chk_pt_list) if x == 'EP'][0]) + 1
        gs_idx = int([i for i, x in enumerate(chk_pt_list) if x == 'GS'][0]) + 1
        starting_epoch = int(chk_pt_list[ep_idx])
        global_step = int(chk_pt_list[gs_idx])
            
        for s in self.schedulers:
            for _ in range(global_step):
                s.step()
        #self.print_networks()
        

        
    def train(self):
        """Make models train mode during train time."""
        for name in self.network_names:
            if isinstance(name, str):
                net = getattr(self, 'net' + name)
            net.train()

    def eval(self):
        """Make models eval mode during test time."""
        for name in self.network_names:
            if isinstance(name, str):
                net = getattr(self, 'net' + name)
            net.eval()

    def validate(self):
        """Forward function used in test time.
        This function wraps <forward> function in no_grad() so we don't save intermediate steps for backprop
        """
        with torch.no_grad():
            self.forward()
            self.backward()
        self.val_loss += getattr(self, 'loss_' + self.network_names[0])
        self.val_step += 1
        self.num_valid_examples += len(self.input[0])
        ## Accumulate input tensors
        [x.extend(y.cpu().data.numpy().tolist()) for x, y in zip(self.val_inputs, self.input)]
        self.val_predictions.extend(self.output)
        self.val_labels.extend(self.label.cpu().data.numpy().tolist())

    def update_learning_rate(self):
        """Update learning rates for all the networks; called at the end of every epoch"""
        for scheduler in self.schedulers:
            scheduler.step()

        lr = self.optimizers[0].param_groups[0]['lr']


    def save_networks(self, epoch, global_step):
        """Save all the networks to the disk.
        """
        
        for name, loss in zip(self.network_names, self.get_current_losses()) :
            if isinstance(name, str):
                save_filename = '{0}_EP_{1}_GS_{2}_ACC_{3:.3}.bin'.format(name, epoch, global_step, float(self.val_acc))
                save_path = self.save_dir / save_filename
                print('Saving model checkpoint at the end of epoch {0} as {1}'.format(epoch, save_path))
                net = getattr(self, 'net' + name)

                if self.use_cuda:
                    torch.save(net.cpu().state_dict(), save_path)
                    net.to(self.device)
                else:
                    torch.save(net.state_dict(), save_path)


    def load_networks(self, checkpoint_name):
        """Load all the networks from the disk.
        """
        print('======== start loading model..chkpt name = {}'.format(checkpoint_name))
        
        for name in self.network_names:
            if isinstance(name, str):
                load_path = self.save_dir / checkpoint_name
                net = getattr(self, 'net' + name)
                if isinstance(net, torch.nn.DataParallel):
                    net = net.module
                print('loading the model from {0}'.format(load_path))
                state_dict = torch.load(load_path, map_location=self.device)
                if hasattr(state_dict, '_metadata'):
                    del state_dict._metadata

                net.load_state_dict(state_dict)


    def save_optimizers(self, epoch, global_step):
        """Save all the optimizers to the disk for restarting training.
        """
        for i, (name, optimizer) in enumerate(zip(self.network_names, self.optimizers)):
            save_filename = '{0}_EP_{1}_GS_{2}_optimizer_{3}.pth'.format(name, epoch, global_step, i)
            save_path = self.save_dir / save_filename
            torch.save(optimizer.state_dict(), save_path)


    def load_optimizers(self, epoch):
        """Load all the optimizers from the disk.
        """
        for i, optimizer in enumerate(self.optimizers):
            load_filename = '{0}_optimizer_{1}.pth'.format(epoch, i)
            load_path = self.save_dir / load_filename
            print('loading the optimizer from {0}'.format(load_path))
            state_dict = torch.load(load_path)
            if hasattr(state_dict, '_metadata'):
                del state_dict._metadata
            optimizer.load_state_dict(state_dict)


    def print_networks(self):
        """Print the total number of parameters in the network and network architecture.
        """
        print('Networks initialized')
        for name in self.network_names:
            if isinstance(name, str):
                net = getattr(self, 'net' + name)
                num_params = 0
                for param in net.parameters():
                    num_params += param.numel()
                print(net)
                print('[Network {0}] Total number of parameters : {1:.3f} M'.format(name, num_params / 1e6))


    def set_requires_grad(self, requires_grad=False):
        """Set requies_grad for all the networks to avoid unnecessary computations.
        """
        for name in self.network_names:
            if isinstance(name, str):
                net = getattr(self, 'net' + name)
                for param in net.parameters():
                    param.requires_grad = requires_grad


    def get_current_losses(self):
        """Return traning losses / errors. train.py will print out these errors on console"""
        errors_ret = OrderedDict()
        for name in self.loss_names:
            if isinstance(name, str):
                errors_ret[name] = float(getattr(self, 'loss_' + name))  # float(...) works for both scalar tensor and float number
        return errors_ret
        

    def pre_epoch_callback(self, epoch):
        pass


    def post_epoch_callback(self, epoch, global_step):
        pass


    def global_step_callback(self, epoch, global_step):
        pass
    
    
    def get_hyperparam_result(self):
        """Returns the final training result for hyperparameter tuning (e.g. best
            validation loss).
        """
        pass


#     def export(self):
#         """Exports all the networks of the model using JIT tracing. Requires that the
#             input is set.
#         """
#         for name in self.network_names:
#             if isinstance(name, str):
#                 net = getattr(self, 'net' + name)
#                 export_path = os.path.join(self.config['export_path'], 'exported_net_{}.pth'.format(name))
#                 if isinstance(self.input, list): # we have to modify the input for tracing
#                     self.input = [tuple(self.input)]
#                 traced_script_module = torch.jit.trace(net, self.input)
#                 traced_script_module.save(export_path)


    def get_current_visuals(self):
        """Return visualization images. train.py will display these images."""
        visual_ret = OrderedDict()
        for name in self.visual_names:
            if isinstance(name, str):
                visual_ret[name] = getattr(self, name)
        return visual_ret
    
    
    
    def evaluate(self, step, n_sample = 2000):
        print('val inputs in evaluate = ', len(self.val_inputs[0]))
        val_loss = self.val_loss / self.num_valid_examples
        val_loss = val_loss.cpu().data.numpy().item()
        
        train_loss = self.train_loss / self.num_train_examples
        train_loss = train_loss.cpu().data.numpy().item()
        
        val_acc = self.calculate_acc(self.val_predictions, self.val_labels)        
        train_acc = self.calculate_acc(self.train_predictions, self.train_labels)
        
        ### Write summary
        tr_summary = {'loss': train_loss, 'acc': train_acc}
        eval_summary = {"loss": val_loss, "acc": val_acc}
        summary = {'train': tr_summary, 'eval': eval_summary}
        
        ### Update summary_manager
        self.summary_manager.update(summary)
        self.summary_manager.save('summary.json')
        
        ### Write Accuracy
        self.tb_writer.add_scalars('acc', {'train': train_acc, 'val': val_acc}, step)
        
        ### Write learning rate
        self.tb_writer.add_scalar('lr', self.scheduler.get_lr()[0], step)
        
        ### Write Loss
        self.tb_writer.add_scalars('loss', {'train': train_loss, 'val': val_loss}, step)
        
 
    def update_train_example(self):
        self.train_loss += getattr(self, 'loss_' + self.network_names[0])
        self.train_step += 1
        self.num_train_examples += len(self.input[0])
        [x.extend(y.cpu().data.numpy().tolist()) for x, y in zip(self.train_inputs, self.input)]
        self.train_predictions.extend(self.output)
        self.train_labels.extend(self.label.cpu().data.numpy().tolist())
        
        
    def calculate_acc(self, pred, label):
        """ evaluate accuracy and return result """
        count_correct = 0
        total_count = 0
        total_sentence=  0
        for i, (p, l) in enumerate(zip(pred, label)):
            count_correct += sum([x == y for x, y in zip (p, l)])  # [CLS], [SEP], [PAD], [MASK] index 모두 포함
            total_count += len(l)
        acc = (count_correct / total_count)  # tensor -> float
        print('acc in calculate_acc = ', acc)
        return acc

    
    def optimize_parameters(self):
        """Calculate gradients and update network weights.
        """
        getattr(self, 'loss_' + self.loss_names[0]).backward() # calculate gradients
        self.optimizer.step()
        self.scheduler.step()
        self.optimizer.zero_grad()
        torch.cuda.empty_cache()