# -*- coding:utf-8 -*-

import torch
from torch import nn
from pytorch_pretrained_bert import BertModel, BertConfig
from torchcrf import CRF
import gluonnlp as nlp


def get_kobert_model(model_file, vocab_file,bert_config, ctx = 'cpu'):
    bertmodel = BertModel(config=BertConfig.from_dict(bert_config))
    bertmodel.load_state_dict(torch.load(model_file))
    device = torch.device(ctx)
    bertmodel.to(device)
    bertmodel.eval()
    vocab_b_obj = nlp.vocab.BERTVocab.from_json(
        open(vocab_file, 'rt').read())
    return bertmodel, vocab_b_obj
        
        
def log_sum_exp(vec):
    max_score = torch.max(vec, 0)[0].unsqueeze(0)
    max_score_broadcast = max_score.expand(vec.size(1), vec.size(1))
    result = max_score + torch.log(torch.sum(torch.exp(vec - max_score_broadcast), 0)).unsqueeze(0)
    return result.squeeze(1)


class Kobert_CRF(nn.Module):
    """
    KOBERT with CRF
    """
    def __init__(self, num_classes, tag_map, model_file, vocab_file, bert_config, ctx = 'cpu') -> None:
        """
        @params:
            model_file: BERT Pretrained Model weights. eg. pytorch_kobert_2439f391a6.params
            vocab_file: BERT vocab file. eg.  kobertvocab_f38b8a4d6d.json
        """
        super(Kobert_CRF, self).__init__()
        self.model_file = model_file
        self.vocab_file = vocab_file
        self.bert_config = bert_config
        self.num_classes = num_classes
        self.tag_map = tag_map
        self.bert, self.vocab = get_kobert_model(self.model_file, self.vocab_file, self.bert_config)
        print('pretrained bert loaded..')
        self.dropout = nn.Dropout(bert_config['hidden_dropout_prob'])
        self.position_wise_ff = nn.Linear(bert_config['hidden_size'], self.num_classes)
        # Register parameter for conditional random field
        self.transitions = nn.Parameter(
            torch.randn(self.num_classes, self.num_classes)
        )
        STOP_TAG = ['[CLS]','[SEP]','[PAD]','[MASK]']

        #print('stop tag = {}'.format([self.tag_map[x] for x in STOP_TAG]))
        self.transitions.data[:, [self.tag_map[x] for x in STOP_TAG]] = -1000.
        self.transitions.data[[self.tag_map[x] for x in STOP_TAG], :] = -1000.
        #print('transition matrix = {}'.format(self.transitions.data))
        self.crf = CRF(num_tags = self.num_classes, batch_first = True)
        
        
    def _get_emission(self, input_ids, token_type_ids, lengths):
        attention_mask = input_ids.ne(self.vocab.token_to_idx[self.vocab.padding_token]).float()
        all_encoder_layers, pooled_output = self.bert(input_ids = input_ids, token_type_ids = token_type_ids, attention_mask = attention_mask)
        last_encoder_layer = all_encoder_layers[-1]
        last_encoder_layer = self.dropout(last_encoder_layer)
        emission = self.position_wise_ff(last_encoder_layer)
        return emission

    def forward(self, input_ids, token_type_ids, lengths, tags = None):
        # token_type_ids: Segment token indices to indicate first and second portions of the inputs. 
        #Indices are selected in [0, 1]: 0 corresponds to a sentence A token, 1 corresponds to a sentence B token 
        #attention_mask Mask to avoid performing attention on padding token indices. Mask values selected in [0, 1]
        emissions = self._get_emission(input_ids, token_type_ids, lengths)
        #scores = []
        #paths = []
        #for logit, leng in zip(emissions, lengths):
        #    logit = logit[:leng]
        #    score, path, probs = self.__viterbi_decode(logit)
        #    scores.append(score)
        #    paths.append(path)
        #return scores, paths, probs

        if tags is not None:
            log_likelihood  = self.crf(emissions, tags)
            sequence_of_tags = self.crf.decode(emissions)
            return log_likelihood, sequence_of_tags
        else:
            sequence_of_tags = self.crf.decode(emissions)
            return sequence_of_tags
        
        
    def __viterbi_decode(self, logits):
        #if self.use_cuda:
        #    trellis = torch.zeros(logits.size()).cuda()
        #    backpointers = torch.zeros(logits.size(), dtype=torch.long).cuda()
        #else:
        trellis = torch.zeros(logits.size())
        backpointers = torch.zeros(logits.size(), dtype=torch.long)

        trellis[0] = logits[0]
        for t in range(1, len(logits)):
            v = trellis[t - 1].unsqueeze(1).expand_as(self.transitions) + self.transitions
            trellis[t] = logits[t] + torch.max(v, 0)[0]
            backpointers[t] = torch.max(v, 0)[1]

        viterbi = [torch.max(trellis[-1], -1)[1].cpu().tolist()]
        backpointers = backpointers.cpu().numpy()

        for bp in reversed(backpointers[1:]):
            viterbi.append(bp[viterbi[-1]])
        viterbi.reverse()

        probs = [torch.nn.Softmax()(trellis[i])[viterbi[i]].detach().tolist() for i in range(len(viterbi))]

        viterbi_score = torch.max(trellis[-1], 0)[0].cpu().tolist()
        return viterbi_score, viterbi, probs
    
    
    def neg_log_likelihood(self, input_ids, token_type_ids, tags, lengths):

        emissions = self._get_emission(input_ids, token_type_ids, lengths)
        real_path_score = torch.zeros(1)
        total_score = torch.zeros(1)

        for logit, tag, leng in zip(emissions, tags, lengths):
            logit = logit[:leng]
            tag = tag[:leng]

            real_path_score += self.real_path_score(logit, tag)
            total_score += self.total_score(logit, tag)
        print('total score: {}, real path score: {}, neg. log-likelihood: {}'.format(total_score, real_path_score, total_score - real_path_score))
        return total_score - real_path_score
    
    
    def real_path_score(self, logits, label):
        '''
        caculate real path score  
        :params logits -> [len_sent * tag_size]
        :params label  -> [1 * len_sent]

        Score = Emission_Score + Transition_Score  
        Emission_Score = logits(0, label[START]) + logits(1, label[1]) + ... + logits(n, label[STOP])  
        Transition_Score = Trans(label[START], label[1]) + Trans(label[1], label[2]) + ... + Trans(label[n-1], label[STOP])  
        '''
        score = torch.zeros(1)
        #label = torch.cat([torch.LongTensor([self.tag_map[START_TAG]]), label])
        for index, logit in enumerate(logits):
            emission_score = logit[label[index]]
            transition_score = self.transitions[label[index], label[index]]
            score += emission_score + transition_score

        score += self.transitions[label[-1], self.tag_map['[CLS]']]

        return score

    
    def total_score(self, logits, label):
        """
        caculate total score
        :params logits -> [len_sent * tag_size]
        :params label  -> [1 * tag_size]
        SCORE = log(e^S1 + e^S2 + ... + e^SN)
        """
        obs = []

        previous = torch.full((1, self.num_classes), 0)
        for index in range(len(logits)): 
            previous = previous.expand(self.num_classes, self.num_classes).t()
            obs = logits[index].view(1, -1).expand(self.num_classes, self.num_classes)
            scores = previous + obs + self.transitions
            previous = log_sum_exp(scores)
        previous = previous + self.transitions[:, self.tag_map['[CLS]']]
        # caculate total_scores
        total_scores = log_sum_exp(previous.t())[0]
        return total_scores
    