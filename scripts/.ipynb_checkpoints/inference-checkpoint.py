import torch
import torch.optim as optim
import os, sys

from models import create_model
from data_manager import DataManager
from utils import load_config, f1_score, get_tags_BIE, CheckpointManager
import copy
import sentencepiece as spm
import json
import numpy as np
import re
from collections import OrderedDict
import argparse
import warnings
from pathlib import Path
from torch.utils.data import DataLoader

from models.NER_model import NERModel
warnings.filterwarnings(action='ignore')


def load_model():
    # Load model
    _model = Kobert_CRF(data_config['num_classes'], 
                       data_config['bert_pretrained_model_file'], 
                       data_config['vocab_file'], 
                       bert_config)
    # State loaded
    checkpoint_dir = Path(train_config['checkpoint_dir'])
    checkpoint_manager = CheckpointManager(checkpoint_dir)
    state = checkpoint_manager.load_checkpoint(data_config['checkpoint_model_file'])
    print('path = {}'.format(data_config['checkpoint_model_file']))
    # Apply state to model
    _model.load_state_dict(state['model_state_dict'])
    return _model
    
def set_dataloader():
    _dm = DataManager(inference_data_file = data_config['inference_data_file'],
                      max_length = train_config['max_length'],
                      tags = data_config['tags'], 
                      tag_map = data_config['tag_map'],
                      tokenizer_model = data_config['tokenizer_model'], 
                      data_type = 'inference') # Need to be 'train' to get information
    _dl = DataLoader(_dm, batch_size=len(_dm), shuffle=False, drop_last=False)
    return _dm, _dl


def NER_decode(path, text, text_tok):
    # Get rid of unnecessary tokens
    detected_tag = [id2tag[tag] for tag in path[:len(text_tok)] if tag not in [tag_map['B-O'], tag_map['I-O'], tag_map['E-O'], 
                                                                               tag_map['[CLS]'], tag_map['[SEP]'], tag_map['[PAD]'], 
                                                                               tag_map['[MASK]']]]
    detected_tag = np.unique([s.split('-')[1] for s in detected_tag]).tolist()
    result = []
    
    for i, tag in enumerate(detected_tag):
        positions = get_tags_BIE(path, tag, tag_map)
        for idx, (s,e) in enumerate(positions):
            entity = dm.tokenizer.DecodePieces(text_tok[s:e+1])
            if len(entity) == 0:
                continue
            else:
                try:
                    entity_pos = re.search(pattern=entity, string=text).span()
                    print('text = ', text, '  entity_pos = ', entity_pos)
                    
                    tmp_result = OrderedDict()
                    tmp_result['entity_type'] = re.sub(pattern = '[<>]+', repl = '', string=tag)    
                    tmp_result['entity_value'] = entity
                    
                    tmp_result['start_pos'] = entity_pos[0]
                    tmp_result['end_pos'] = entity_pos[1]
                    #tmp_result['score'] = round(prob, 2)
                    tmp_result['isPredict'] = True
                    result.append(tmp_result)
                except:
                    continue
    return result
    
def inference(dm, model):    
    x_input = []
    token_type_ids = []
    origin_sent = []
    sentence_piece = []
    
    for d in dm:
        x_input.append(d[0].data.numpy())
        token_type_ids.append(d[1].data.numpy())
        origin_sent.append(d[4])
        sentence_piece.append(d[5])
    
    print(torch.tensor(x_input))
    path = model(input_ids = torch.tensor(x_input), token_type_ids = torch.tensor(token_type_ids))
    result = [NER_decode(path[k], origin_sent[k], sentence_piece[k]) for k in range(len(path))]
    out = {}
    out['sentences'] = origin_sent
    out['result'] = result
    return out


def set_output_file(output_path, input_file_name):    
    output_file_name = input_file_name.split('/')[-1].replace('.json', '') + '_result.json'
    output_dir = Path(output_path)
    return output_dir / output_file_name


if __name__ == '__main__':

    # Load Config
    config = load_config()
    
    # Load data
    te_dl = create_dataloader(config['inference_dataset_params'])
 
    # Load model
    model = create_model(config['model_params'], config['train_params'], bert_config = config['bert_config'], t_total = t_total)
    
    # Set output file
    output_file = set_output_file(train_config['inference_output_dir'], data_config['inference_data_file'])
    
    tag_map = dm.tag_map
    tags = dm.tags
    id2tag = {v:k for k, v in dm.tag_map.items()}
    
    inference_result = inference(dm, model)

    with open(output_file, 'w', encoding='utf-8') as file:
        json.dump(inference_result, file, ensure_ascii=False)

    #for i, r in enumerate(result):
    #    print('Input text: {}'.format(text[i]))
    #    print('Results:')
    #    for entities in r:
    #        print(entities)
    #    print('-'*30)