# -*- coding:utf-8 -*-
from __future__ import absolute_import, division, print_function, unicode_literals

from models.base_model import BaseModel

import torch

from pytorch_transformers import AdamW, WarmupLinearSchedule
from torch import nn
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence

from pytorch_pretrained_bert import BertModel, BertConfig

from torchcrf import CRF
import gluonnlp as nlp


def get_kobert_model(model_file, vocab_file,bert_config, ctx = 'cpu'):
    bertmodel = BertModel(config=BertConfig.from_dict(bert_config))
    bertmodel.load_state_dict(torch.load(model_file))
    device = torch.device(ctx)
    bertmodel.to(device)
    bertmodel.eval()
    vocab_b_obj = nlp.vocab.BERTVocab.from_json(
        open(vocab_file, 'rt').read())
    return bertmodel, vocab_b_obj
        

def log_sum_exp(vec):
    max_score = torch.max(vec, 0)[0].unsqueeze(0)
    max_score_broadcast = max_score.expand(vec.size(1), vec.size(1))
    result = max_score + torch.log(torch.sum(torch.exp(vec - max_score_broadcast), 0)).unsqueeze(0)
    return result.squeeze(1)

class Kobert_CRF(nn.Module):
    """
    KOBERT with CRF
    """
    def __init__(self, num_classes, tag_map, model_file, vocab_file, bert_config, ctx = 'cpu') -> None:
        """
        @params:
            model_file: BERT Pretrained Model weights. eg. pytorch_kobert_2439f391a6.params
            vocab_file: BERT vocab file. eg.  kobertvocab_f38b8a4d6d.json
        """
        super(Kobert_CRF, self).__init__()
        self.model_file = model_file
        self.vocab_file = vocab_file
        self.bert_config = bert_config
        self.num_classes = num_classes
        self.tag_map = tag_map
        self.bert, self.vocab = get_kobert_model(self.model_file, self.vocab_file, self.bert_config)
        print('pretrained bert loaded..')
        self.dropout = nn.Dropout(bert_config['hidden_dropout_prob'])
        self.position_wise_ff = nn.Linear(bert_config['hidden_size'], self.num_classes)
        # Register parameter for conditional random field
        self.transitions = nn.Parameter(
            torch.randn(self.num_classes, self.num_classes)
        )
        STOP_TAG = ['[CLS]','[SEP]','[PAD]','[MASK]']

        #print('stop tag = {}'.format([self.tag_map[x] for x in STOP_TAG]))
        self.transitions.data[:, [self.tag_map[x] for x in STOP_TAG]] = -1000.
        self.transitions.data[[self.tag_map[x] for x in STOP_TAG], :] = -1000.
        #print('transition matrix = {}'.format(self.transitions.data))
        #elf.crf = CRF(num_tags = self.num_classes, batch_first = True)
        
        
    def _get_emission(self, input_ids, token_type_ids, lengths):
        if not lengths:
            lengths = [i.size(-1) for i in input_ids]
        
        attention_mask = input_ids.ne(self.vocab.token_to_idx[self.vocab.padding_token]).float()
        #print('attention mask shape = {}'.format(attention_mask.shape))
        #print('input_ids shape = {}'.format(input_ids.shape))
        #print('token_type_ids shape = {}'.format(token_type_ids.shape))
        all_encoder_layers, pooled_output = self.bert(input_ids = input_ids, token_type_ids = token_type_ids, attention_mask = attention_mask)
        last_encoder_layer = all_encoder_layers[-1]
        last_encoder_layer = self.dropout(last_encoder_layer)
        emission = self.position_wise_ff(last_encoder_layer)
        return emission
    

    def forward(self, input_ids, token_type_ids, lengths = None):
        # token_type_ids: Segment token indices to indicate first and second portions of the inputs. 
        #Indices are selected in [0, 1]: 0 corresponds to a sentence A token, 1 corresponds to a sentence B token 
        #attention_mask Mask to avoid performing attention on padding token indices. Mask values selected in [0, 1]
        
        emissions = self._get_emission(input_ids, token_type_ids, lengths)
        scores = []
        paths = []
        for logit, leng in zip(emissions, lengths):
            logit = logit[:leng]
            score, path, probs = self.__viterbi_decode(logit)
            scores.append(score)
            paths.append(path)
        return scores, paths, probs

        #if tags is not None:
        #      = self.crf(emissions, tags)
        #    sequence_of_tags = self.crf.decode(emissions)
        #    return log_likelihood, sequence_of_tags
        #else:
        #    sequence_of_tags = self.crf.decode(emissions)
        #    return sequence_of_tags
        
    def __viterbi_decode(self, logits):
        #if self.use_cuda:
        #    trellis = torch.zeros(logits.size()).cuda()
        #    backpointers = torch.zeros(logits.size(), dtype=torch.long).cuda()
        #else:
        trellis = torch.zeros(logits.size())
        backpointers = torch.zeros(logits.size(), dtype=torch.long)

        trellis[0] = logits[0]
        for t in range(1, len(logits)):
            v = trellis[t - 1].unsqueeze(1).expand_as(self.transitions) + self.transitions
            trellis[t] = logits[t] + torch.max(v, 0)[0]
            backpointers[t] = torch.max(v, 0)[1]

        viterbi = [torch.max(trellis[-1], -1)[1].cpu().tolist()]
        backpointers = backpointers.cpu().numpy()

        for bp in reversed(backpointers[1:]):
            viterbi.append(bp[viterbi[-1]])
        viterbi.reverse()

        probs = [torch.nn.Softmax()(trellis[i])[viterbi[i]].detach().tolist() for i in range(len(viterbi))]

        viterbi_score = torch.max(trellis[-1], 0)[0].cpu().tolist()
        return viterbi_score, viterbi, probs
    
    
    def neg_log_likelihood(self, input_ids, token_type_ids, tags, lengths = None):
        if not lengths:
            lengths = [i.size(-1) for i in input_ids]
        emissions = self._get_emission(input_ids, token_type_ids, lengths)
        real_path_score = torch.zeros(1)
        total_score = torch.zeros(1)

        for logit, tag, leng in zip(emissions, tags, lengths):
            logit = logit[:leng]
            #print(logit)
            tag = tag[:leng]

            real_path_score += self.real_path_score(logit, tag)
            total_score += self.total_score(logit, tag)
        print('total score: {}, real path score: {}, neg. log-likelihood: {}'.format(total_score, real_path_score, total_score - real_path_score))
        return total_score - real_path_score
    
    
    def real_path_score(self, logits, label):
        '''
        caculate real path score  
        :params logits -> [len_sent * tag_size]
        :params label  -> [1 * len_sent]

        Score = Emission_Score + Transition_Score  
        Emission_Score = logits(0, label[START]) + logits(1, label[1]) + ... + logits(n, label[STOP])  
        Transition_Score = Trans(label[START], label[1]) + Trans(label[1], label[2]) + ... + Trans(label[n-1], label[STOP])  
        '''
        score = torch.zeros(1)
        #label = torch.cat([torch.LongTensor([self.tag_map[START_TAG]]), label])
        for index, logit in enumerate(logits):
            emission_score = logit[label[index]]
            transition_score = self.transitions[label[index], label[index]]
            score += emission_score + transition_score

        score += self.transitions[label[-1], self.tag_map['[CLS]']]

        return score

    
    def total_score(self, logits, label):
        """
        caculate total score
        :params logits -> [len_sent * tag_size]
        :params label  -> [1 * tag_size]
        SCORE = log(e^S1 + e^S2 + ... + e^SN)
        """
        obs = []

        previous = torch.full((1, self.num_classes), 0)
        for index in range(len(logits)): 
            previous = previous.expand(self.num_classes, self.num_classes).t()
            obs = logits[index].view(1, -1).expand(self.num_classes, self.num_classes)
            scores = previous + obs + self.transitions
            previous = log_sum_exp(scores)
        previous = previous + self.transitions[:, self.tag_map['[CLS]']]
        # caculate total_scores
        total_scores = log_sum_exp(previous.t())[0]
        return total_scores
    
    

        
class NERModel(BaseModel):
    def __init__(self, model_config, train_config, **kwargs):
        """
        Initialize the model.
        """
        super().__init__(model_config, train_config)
        ## Parameters
        self.num_classes = model_config['num_classes']
        self.tag_map = model_config['tag_map']
        self.bert_pretrained_model_file = model_config['bert_pretrained_model_file']
        self.vocab_file = model_config['vocab_file']
        self.bert_config = kwargs['bert_config']
        self.gradient_accumulation_steps = model_config['gradient_accumulation_steps']
        self.learning_rate = model_config['learning_rate']
        self.adam_eps = model_config['adam_epsilon']
        self.warmup_steps = model_config['warmup_steps']
        self.epochs = model_config['epochs']
        self.device = torch.device('cpu') # 일단 하드코딩
        self.loss_names = ['ner']
        self.network_names = ['NER']
        self.t_total = kwargs['t_total']
        
        ## Define network
        self.netNER = Kobert_CRF(self.num_classes, self.tag_map, self.bert_pretrained_model_file, self.vocab_file, self.bert_config)
        self.netNER = self.netNER.to(self.device)

        # Define optimizer & scheduler
        if self.is_train:  # only defined during training time
            self.optimizer = self.define_optimizer()
            self.scheduler = self.define_scheduler()
            self.optimizers = [self.optimizer]

        # storing predictions and labels for validation
        self.val_predictions = []
        self.val_labels = []
        self.val_images = []


    def forward(self):
        """Run forward pass.
        """
        self.output = self.netNER.neg_log_likelihood(*self.input, self.label)
     
        

    def backward(self):
        """Calculate losses; called in every training iteration.
        """
        self.loss_ner = self.output
        

    def optimize_parameters(self):
        """Calculate gradients and update network weights.
        """
        self.loss_ner.backward() # calculate gradients
        self.optimizer.step()
        self.optimizer.zero_grad()
        torch.cuda.empty_cache()


        
    def set_input(self, input):
        """
        Unpack input data from the dataloader and perform necessary pre-processing steps.
        The implementation here is just a basic setting of input and label. You may implement
        other functionality in your own model.
        """
        x_input, token_type_ids, y_real, length = map(lambda elm: elm.to(self.device), input[:4])
        #print('x_input type = {}'.format((x_input)))
        #print('token_type_ids type = {}'.format((token_type_ids)))
        self.input = [x_input, token_type_ids]
        self.label = torch.tensor(y_real)
        
        
    def test(self):
        super().test() # run the forward pass

        # save predictions and labels as flat tensors
        self.val_images.append(self.input)
        self.val_predictions.append(self.output)
        self.val_labels.append(self.label)


    def post_epoch_callback(self, epoch, visualizer):
        self.val_predictions = torch.cat(self.val_predictions, dim=0)
        predictions = torch.argmax(self.val_predictions, dim=1)
        predictions = torch.flatten(predictions).cpu()

        self.val_labels = torch.cat(self.val_labels, dim=0)
        labels = torch.flatten(self.val_labels).cpu()

        self.val_images = torch.squeeze(torch.cat(self.val_images, dim=0)).cpu()

        # Calculate and show accuracy
        val_accuracy = accuracy_score(labels, predictions)

        metrics = OrderedDict()
        metrics['accuracy'] = val_accuracy

        visualizer.plot_current_validation_metrics(epoch, metrics)
        print('Validation accuracy: {0:.3f}'.format(val_accuracy))

        # Here you may do something else with the validation data such as
        # displaying the validation images or calculating the ROC curve

        self.val_images = []
        self.val_predictions = []
        self.val_labels = []
        
    
    def define_optimizer(self):
        """
        decide parameters to learn
        define scheduler if want
        Use 1 gpu for default
        """
        param_optimizer = list(self.netNER.named_parameters())
        print("Number of parameter matrix = ", len(param_optimizer))

        no_decay = ['bias', 'LayerNorm.bias', 'LayerNorm.weight']
        optimizer_grouped_parameters = [
            {'params': [p for n, p in param_optimizer if not any(nd in n for nd in no_decay)], 'weight_decay': .01},
            {'params': [p for n, p in param_optimizer if any(nd in n for nd in no_decay)], 'weight_decay': .00},
        ]

        # Optimizer
        _opt = AdamW(optimizer_grouped_parameters, lr = self.learning_rate, eps = self.adam_eps)
        
        return _opt
    
    
    def define_scheduler(self):
        # WarmupLinearSchedule: Linearly increases learning rate from 0 to 1 over warmup fraction of training steps. Linearly decreases learning rate from 1. to 0. over remaining 1
        return WarmupLinearSchedule(self.optimizer, warmup_steps = self.warmup_steps, t_total = self.t_total)