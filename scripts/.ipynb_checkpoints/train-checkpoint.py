# -*- coding:utf-8 -*-

# Architecture modified from
# https://github.com/branislav1991/PyTorchProjectFramework/blob/master/models/segmentation_model.py
import torch
import torch.optim as optim
from pytorch_transformers import AdamW, WarmupLinearSchedule
from torch.utils.tensorboard import SummaryWriter
from utils import CheckpointManager, SummaryManager

from datasets import create_dataloader
from models import create_model
from trainers import Trainer
from utils import set_seed
import os
import sys
from utils import load_config, f1_score, save_cr_and_cm

#from kobert_crf_model import Kobert_CRF
import logging
import json
from tqdm import tqdm, trange
from pathlib import Path
import numpy as np
import argparse


logger = logging.getLogger()
stderr_log_handler = logging.StreamHandler()
logger.addHandler(stderr_log_handler)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
stderr_log_handler.setFormatter(formatter)


    
if __name__ == '__main__':
    
    # Load Config
    config = load_config()
    
    # Set DataLoader (load from ./datasets/NERWOZ3_dataset.py)
    tr_dl = create_dataloader(config['train_dataset_params'])
    va_dl = create_dataloader(config['valid_dataset_params'])

    t_total = len(tr_dl) // config['model_params']['gradient_accumulation_steps'] * config['model_params']['epochs']
    
    
    # Define Model => affects trainer
    model = create_model(config['model_params'], config['train_params'], bert_config = config['bert_config'], t_total = t_total)
    
    # Train
    trainer = Trainer(tr_dl, va_dl, model, config)
    trainer()