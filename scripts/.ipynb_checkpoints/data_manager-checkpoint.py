# -*- coding:utf-8 -*-

from __future__ import absolute_import, division, print_function, unicode_literals

import codecs
import os
import torch
from sklearn.model_selection import train_test_split
from torch.utils.data import Dataset

import numpy as np
import pandas as pd
from pprint import pprint
from typing import Tuple, Callable, List
import pickle
import json
from tqdm import tqdm
from collections import OrderedDict
import re
import copy
import sentencepiece as spm
from pathlib import Path


class DataManager(Dataset):
    def __init__(self, 
                 train_data_file = None, 
                 inference_data_file = None,
                 max_length = 30, 
                 tags = None, 
                 tag_map = None,
                 tokenizer_model = None, 
                 data_type='train'):
        
        self.train_data_file = train_data_file
        self.inference_data_file = inference_data_file
        self.max_length = max_length
        self.tags = tags
        self.tag_map = tag_map
        self.tokenizer_model = tokenizer_model
        self.data_type = data_type
        self.data = []
        self.batch_data = []
        self.original_sentence = []
        self.original_sentence_piece = []

        self.tokenizer = spm.SentencePieceProcessor()
        self.tokenizer.Load(self.tokenizer_model)

        if self.data_type in ['train', 'valid']:
            self.load_data()
        elif self.data_type == 'inference':
            self.load_data_for_inference()
        
    def __len__(self) -> int:
        return len(self.data)
    
    def __getitem__(self, idx: int):
        _data = self.pad_data(self.data[idx])
        _len = len(self.data[idx][0]) # 원래 문장의 길이

        _input = [self.tokenizer.bos_id()] + _data[0] + [self.tokenizer.eos_id()]
        _input = torch.tensor(_input).long()
        token_type_ids = torch.tensor(len(_input) * [0])
        if self.data_type in ['train', 'valid']:
            _label = [self.tag_map['[CLS]']] + _data[1] + [self.tag_map['[SEP]']]
            _label = torch.tensor(_label).long()
            return _input, token_type_ids, _label, _len, self.original_sentence[idx], self.original_sentence_piece[idx]
        else:
            return _input, token_type_ids, [], _len, self.original_sentence[idx], self.original_sentence_piece[idx]
    
        
    def load_data(self):
        """
        Load data with keys: train, valid, categories
        """
        assert(self.train_data_file is not None)
        with open(self.train_data_file, 'r', encoding='utf-8') as f:
            loaded = json.load(f)
            
        key_list = [k for k in list(loaded.keys()) if k != 'categories']

        if self.data_type == 'train':
            dataset = loaded['train']

        elif self.data_type == 'valid':
            dataset = loaded['valid']
            
        for d in dataset:
            sent, tag = d['sentence'], d['tags']
            
            sentence = [] # Token IDs
            sentence_piece = [] # Token Pieces
            target = []
            
            for i, (s, t) in enumerate(zip(sent, tag)):
                tok_sent = self.tokenizer.EncodeAsIds(s)
                tok_sent_piece = self.tokenizer.EncodeAsPieces(s)
                sentence += tok_sent
                sentence_piece += tok_sent_piece
                if len(tok_sent) == 0:
                    target += []
                elif len(tok_sent) == 1:
                    target += [self.tag_map['B-' + t]]
                elif len(tok_sent) == 2:
                    target += [self.tag_map["B-" + t], self.tag_map['E-'+t]]
                elif len(tok_sent) >= 3:
                    target += [self.tag_map["B-"+t]] + [self.tag_map['I-'+t]] * (len(tok_sent)-2) + [self.tag_map['E-'+t]]
                    
            self.data.append([sentence, target])
            self.original_sentence.append(d['original_sentence'])
            self.original_sentence_piece.append(sentence_piece)
        
        self.input_size = self.tokenizer.get_piece_size()
        print("=" * 30)
        print("{} data: {}".format(self.data_type, len(self.data)))
        print("vocab size: {}".format(self.input_size))
        print("unique tag: {}".format(len(self.tag_map.values())))
        print("=" * 30)
        
        
    def load_data_for_inference(self):
        """
        Load data with key: sentences
        """
        assert(self.inference_data_file is not None)
        with open(self.inference_data_file, 'r', encoding='utf-8') as f:
            loaded = json.load(f)

        dataset = loaded['sentences']
        print('data set = {}'.format(dataset[0]))
        for d in dataset:   
            sentence = self.tokenizer.EncodeAsIds(d)
            sent_piece = self.tokenizer.EncodeAsPieces(d)
            self.data.append([sentence])
            self.original_sentence.append(d)
            self.original_sentence_piece.append(sent_piece)
        
        self.input_size = self.tokenizer.get_piece_size()
        print("=" * 30)
        print("{} data: {}".format(self.data_type, len(self.data)))
        print("vocab size: {}".format(self.input_size))

        print("=" * 30)
        
        
    def pad_data(self, data): # 원래 버전에는 batch 단위로 max_length를 구해서 padding하였으나, 1 문장을 기준으로 정해진 max_lenth에 따라 padding
        """
        @return:
            input, 
            label, 
            original_length
        """
        c_data = copy.deepcopy(data)
        c_data.append(len(c_data))
        if len(c_data[0]) > self.max_length - 2:
            c_data[0] = c_data[0][:self.max_length - 2]
            if self.data_type != 'inference':
                c_data[1] = c_data[1][:self.max_length - 2]
        else:
            c_data[0] = c_data[0] + (self.max_length - len(c_data[0]) - 2) * [self.tag_map['[PAD]']]
            if self.data_type != 'inference':
                c_data[1] = c_data[1] + (self.max_length - len(c_data[1]) - 2) * [self.tag_map['[PAD]']]

        return c_data