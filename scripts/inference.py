# -*- coding:utf-8 -*-

import torch
import torch.optim as optim
import os, sys, time, json, warnings
import sentencepiece as spm
from model import create_model
from datasets import create_dataloader
from utils import load_config
from utils.ner_util import NER_decode
from model.NER_model import NERModel
from tqdm import tqdm, trange

from pathlib import Path


warnings.filterwarnings(action='ignore')


def inference(dl, model, id2tag, tag_map):
    tokenizer = spm.SentencePieceProcessor()
    tokenizer.Load(config['model_params']['tokenizer_model'])
    epoch_start_time = time.time()  # timer for entire epoch
    epoch_iterator = tqdm(dl, desc="Iteration")
    
    # Callback
    model.eval()
    _origin_sent, _result = [], []
    for i, data in enumerate(epoch_iterator):  # inner loop within one epoch   
        model.set_input(data)         # unpack data from dataset and apply preprocessing
        model.forward()
        origin_sent = data[4]
        tokens = [tokenizer.EncodeAsPieces(x) for x in origin_sent]
        result = [NER_decode(x, y, z, id2tag, tag_map, tokenizer) for x, y, z in zip(model.output, origin_sent, tokens)]
    return result


def set_output_file(output_path, input_file_name):    
    output_file_name = input_file_name.split('/')[-1].replace('.json', '') + '_result.json'
    output_dir = Path(output_path)
    output_dir.mkdir(parents= True, exist_ok = True)
    return output_dir / output_file_name


if __name__ == '__main__':

    # Load Config
    config = load_config()
    
    # Load data
    te_dl = create_dataloader(config['inference_dataset_params'], tag_info = config['tag_info'])

    # Load model
    model = create_model(config['model_params'], config['train_params'], bert_config = config['bert_config'], tag_info = config['tag_info'])
    model.load_networks(config['model_params']['checkpoint_model_file'])
    # Set output file
    output_file = set_output_file(config['train_params']['inference_output_dir'], config['inference_dataset_params']['dataset_name'])
    

    id2tag = {v:k for k, v in config['tag_info']['tag_map'].items()}

    inference_result = inference(te_dl, model, id2tag, config['tag_info']['tag_map'])

    with open(output_file, 'w', encoding='utf-8') as file:
        json.dump(inference_result, file, ensure_ascii=False)