

# Learning framework

## Purpose
  - For easy experimentation of various setups
  - Structurize scripts to assetize
  
## Notes
  - Automatically calculate basic metrics such as train/valid loss/accuracy
  - Additional metrics can be manually
  - Metric evaluations are done on CPU via list
  - Metrics are visualzied using tensorboard
  - Assumes multiple networks with multiple losses and optimizers
  
 
